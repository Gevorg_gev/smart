using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication20
{
    [ApiController]
    [Route("[controller]")]
    public class SmartphoneController : ControllerBase
    {
        public SmartphoneController(SmartphoneDbContext smartContext)
        {
            this.smartContext = smartContext;
        }

        private static readonly string[] Smartphone_Brand = new[]
        {
            "Samsung" , "Apple" , "Huawei" , "Xiaomi" , "Meizu" , "Alcatel"
        };

        private static readonly string[] Smartphone_Name = new[]
        {
            "Samsung Galaxy A7(2017)" , "Apple Iphone 12 Pro Max" , "Huawei X5" ,
            "Xiaomi Mi Note 10 Lite" , "Meizu M4" , "Alcatel OneTouch 5"
        };

        private static readonly string[] Smartphone_Color = new[]
        {
            "Matt Black" , "Space Gray" , "Midhnight Green" , "Ceramic White" , "Black" , "Red" 
        };

        private static readonly string[] Smartphone_Country = new[]
        {
            "South Korea" , "United States Of America" , "China" , "China" , "China" , "Thailand" 
        };

        private SmartphoneDbContext smartContext;
        [HttpGet]
        public IEnumerable<Smartphone> Get() 
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new Smartphone
            {
                Id = rng.Next(1,5),
                Brand = Smartphone_Brand[rng.Next(Smartphone_Brand.Length)],
                Name = Smartphone_Name[rng.Next(Smartphone_Name.Length)],
                Color = Smartphone_Color[rng.Next(Smartphone_Color.Length)],
                Country = Smartphone_Country[rng.Next(Smartphone_Country.Length)] 
            });
        } 

        [HttpPost]
        public IActionResult AddSmartphone([FromBody] Smartphone smartphone)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            smartContext.Smartphones.Add(smartphone);
            smartContext.SaveChanges();

            return Ok(); 
        }

        [HttpDelete("{Id}")]
        public IActionResult RemoveSmarftphone([FromRoute] int id)
        {
            var sm = smartContext.Smartphones.Find(id);
            if(sm == null)
            {
                return BadRequest("Wrong Smartphone Id!\n Please try again...");
            }

            smartContext.Smartphones.Remove(sm);
            smartContext.SaveChanges();

            return Ok(); 
        } 
    }
}
