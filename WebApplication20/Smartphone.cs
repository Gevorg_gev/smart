namespace WebApplication20
{
    public class Smartphone
    {
        public int Id { get; set; }
        public string Brand { get; set; } 
        public string Name { get; set; }
        public string Color { get; set; }
        public string Country { get; set; }
    }
}